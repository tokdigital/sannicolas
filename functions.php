<?php

add_theme_support( 'menus' );
add_theme_support( 'widgets' );
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 600, 400, array( 'center', 'center') );
add_image_size( 'home_slider', 1440, 810, true );
add_image_size( 'gallery', 800, 500, true );
add_image_size( 'square', 600, 600, true );
 


 function tok_scripts() {
 
    wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', '', '', false);
    wp_enqueue_style( 'lightbox2-stylesheet', get_stylesheet_directory_uri() . '/node_modules/lightbox2/dist/css/lightbox.min.css', '', '', false);
    wp_enqueue_style( 'swiperjs', 'https://unpkg.com/swiper@7/swiper-bundle.min.css', '', '', false);
    wp_enqueue_style( 'aos', get_stylesheet_directory_uri() . '/node_modules/aos/dist/aos.css', '', '', false);
    wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/css/app.css?v' . time(), '', '', false);

    wp_deregister_script('jquery');

    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri(). '/node_modules/jquery/dist/jquery.min.js', array(), '', false);

    wp_enqueue_script( 'what-input', get_stylesheet_directory_uri(). '/node_modules/what-input/dist/what-input.min.js', array(), '', true);
    wp_enqueue_script( 'aos', get_stylesheet_directory_uri(). '/node_modules/aos/dist/aos.js', array(), '', true);
    wp_enqueue_script( 'foundation', get_stylesheet_directory_uri(). '/node_modules/foundation-sites/dist/js/foundation.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'swiperjs', 'https://unpkg.com/swiper@7/swiper-bundle.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'tok-scripts', get_stylesheet_directory_uri(). '/js/app.js?v' . time(), array(), '', true );

}
add_action( 'wp_enqueue_scripts', 'tok_scripts' );

 
