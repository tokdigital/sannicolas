<?php /* Template Name: Location */ ?>

<?php get_header(); ?>

<section class="section-location">
	<div class="grid-container">
		<div class="grid-x align-middle">
            <div class="cell medium-8" data-aos="fade-up" data-aos-duration="2500">
                <div id="map" class="map"></div>
            </div>
            <div class="cell medium-4" data-aos="fade-up" data-aos-duration="2500" data-aos-delay="700">
                <article class="location-description">
                    <h1>Vlora</h1>
                    <p>Vlorë is the third most populous city of the Republic of Albania and the capital of the eponymous county and municipality. Geographically, the city is located on the Bay of Vlorë and the foothills of the Ceraunian Mountains at the Strait of Otranto along the Adriatic and Ionian Sea within the Mediterranean Sea.</p>
                    <div class="grid-x lat-long">
                        <div class="cell medium-6">
                            40.4661° N
                        </div>
                        <div class="cell medium-6 text-right">
                            19.4914° E
                        </div>
                    </div>
                </article>
            </div>
        </div>
	</div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDB63CSNJRcYHZQj7DlygHP6FfHDNLqm5Q&callback=initMap&libraries=&v=weekly" async></script>

<script>
function initMap() {
  
  var isDraggable = $(document).width() > 640 ? true : false; // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false
  var centerPosition = {lat: 40.163981, lng: 19.600626};
  
  var map = new google.maps.Map(document.getElementById('map'), {
      center: centerPosition,
      zoom: 8,
  });
  
  var marker = new google.maps.Marker({
      position: centerPosition,
      icon: {
        url: 'http://sannicolas.tokdev.al/wp-content/themes/sannicolas/img/map-marker-logo.png',
        scaledSize: new google.maps.Size(70, 70)
    }
  });
  map.setOptions({
    draggable: isDraggable,
    scrollwheel: false
  });
  marker.setMap(map);

}
</script>

<?php get_footer(); ?>
