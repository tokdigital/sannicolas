<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/img/favicon.png" type="image/png" sizes="16x16">
    <title><?php the_title(); ?> - San Nicolas</title> 
    <?php wp_head() ?>
</head>
<body <?php body_class(); ?>>
     
<header class="header" id="scroll-menu">

    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell medium-3">
                <a href="<?php echo site_url() ?>"><img class="logo" src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a>
            </div>
            <div class="cell medium-9 text-right show-for-medium">
                <?php 
                wp_nav_menu(array(
                    'container' => 'nav'
                ));
                ?>
            </div>
        </div>
    </div>

</header>


<input type="checkbox" class="hide-for-medium" id="overlay-input" />
<label for="overlay-input" id="mobile-overlay-button" class="hide-for-medium"><span></span></label>
<div id="mobile-overlay" class="mobile-overlay">
    <img class="logo-mobile" src="<?php bloginfo('template_url'); ?>/img/logo-black.svg">
    <?php 
    wp_nav_menu();
    ?>
</div>