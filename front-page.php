<?php get_header(); ?>

<section class="section-home">
	<div class="grid-container">
		<div class="grid-x align-middle">
			<div class="cell medium-6 show-for-medium" data-aos="fade-up"  data-aos-duration="2500">
				<h1>PARADISE<br>ON EARTH</h1>
				<div class="learn-more" id="learn-more">
					<h6>LEARN MORE</h6>
					<h5>View our promo video</h5>
					<i class="fas fa-long-arrow-alt-right"></i>
				</div>
			</div>
			<div class="cell medium-5 medium-offset-1" data-aos="fade-up"  data-aos-duration="2500" data-aos-delay="700">
				<figure>
					<img src="<?php bloginfo('template_url'); ?>/img/the-project.png" alt="San Nicolas Residences">
					<div class="play-button-wrapper" data-aos="fade-in" data-aos-duration="2500" data-aos-delay="2000">
						<button id="play-button" class="play-button"></button>
					</div>
				</figure>
			</div>
			<h2 class="hide-for-medium" data-aos="fade-up" data-aos-duration="2500" data-aos-delay="1400">PARADISE ON EARTH</h2>
		</div>
	</div>
</section>

<div class="video-overlay" id="video-overlay">
	<div class="inner">
		<div class="close-button">&times;</div>
		<video src="<?php bloginfo('template_url'); ?>/img/video.mp4" id="video" controls></video>
	</div>
</div>

<div class="preloader">
	<svg width="200" height="200" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ripple" style="background:0 0"><circle cx="50" cy="50" r="4.719" fill="none" stroke="#1d3f72" stroke-width="2"><animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="3" keySplines="0 0.2 0.8 1" begin="-1.5s" repeatCount="indefinite"/><animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="3" keySplines="0.2 0 0.8 1" begin="-1.5s" repeatCount="indefinite"/></circle><circle cx="50" cy="50" r="27.591" fill="none" stroke="#5699d2" stroke-width="2"><animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="3" keySplines="0 0.2 0.8 1" begin="0s" repeatCount="indefinite"/><animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="3" keySplines="0.2 0 0.8 1" begin="0s" repeatCount="indefinite"/></circle></svg>
</div>

<?php get_footer(); ?>
