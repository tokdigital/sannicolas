<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>





<section class="about-us" style="padding-top: 80px;">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6" data-equalizer-watch="medium">
				<div class="about-content wow fadeInLeft"  data-wow-delay="0.2s">
					<div class="inner">
						<h1 class="section-title"><?php the_title(); ?></h1>
						<div class="description">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="section-image wow fadeInRight"  data-wow-delay="0.2s">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
			<?php if ( get_field('extra_content')) {?>
			<div class="cell wow fadeInLeft"  data-wow-delay="0.2s">
				<div class="description extra"><?php the_field('extra_content'); ?></div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>