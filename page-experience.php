<?php /* Template Name: The Experience */ ?>

<?php get_header(); ?>

<section class="section-experience">
	<div class="grid-container">
		<div class="grid-x align-middle">
            <div class="cell medium-6" data-aos="fade-up" data-aos-duration="2500">

                <!-- Slider main container -->
                <div class="swiper" id="image-slider">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <img class="slide-image" src="<?php bloginfo('template_url'); ?>/img/slide1.png" alt="">
                            <img class="slide-decor" src="<?php bloginfo('template_url'); ?>/img/flower-green.png" alt="">
                            <h1>HARMONY</h1>
                        </div>
                        <div class="swiper-slide">
                            <img class="slide-image" src="<?php bloginfo('template_url'); ?>/img/slide2.png" alt="">
                            <img class="slide-decor" src="<?php bloginfo('template_url'); ?>/img/flower-red.png" alt="">
                            <h1>SERENITY</h1>
                        </div>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div><!-- swiper -->

            </div>
            <div class="cell medium-5 medium-offset-1" data-aos="fade-up" data-aos-duration="2500" data-aos-delay="700">

                <!-- Slider main container -->
                <div class="swiper" id="text-slider">
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <h2>Sometimes</h2>
                            <p>all it takes is a breath to lift you up and carry 
                            you away to a place with more questions than 
                            answers where your imagination can run free 
                            and everything lives and breathes in the moment. 
                            And all that matters is in front of you.</p>
                        </div>
                        <div class="swiper-slide">
                            <h2>Sometimes</h2>
                            <p>all it takes is a breath to lift you up and carry 
                            you away to a place with more questions than 
                            answers where your imagination can run free 
                            and everything lives and breathes in the moment. 
                            And all that matters is in front of you.</p>
                        </div>
                    </div>
                </div><!-- swiper -->

            </div>
        </div>
	</div>
</section>

<div class="video-overlay" id="video-overlay">
	<div class="inner">
		<div class="close-button">&times;</div>
		<video src="<?php bloginfo('template_url'); ?>/img/video.mp4" id="video" controls></video>
	</div>
</div>

<?php get_footer(); ?>
