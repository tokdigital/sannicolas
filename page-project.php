<?php /* Template Name: The Project */ ?>

<?php get_header(); ?>

<div class="hero-section">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-5">
				<div class="content">
					<div class="default-title">PARADISE<br>ON EARTH</div>
					<p>info@sannicolas.al<br>+355694022660</p>
					<div class="socials">
						<a href="https://www.facebook.com/San-Nicolas-Resort-Residences-106423758390736" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" class="social-image"></a>
						<a href="https://www.instagram.com/sannicolas.al/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/insta.svg" class="social-image" target="_blank"></a>
					</div>
				</div>
			</div>
			<div class="cell medium-1"></div>
			<div class="cell medium-5">
				<div class="form-holder">
					<?php echo do_shortcode('[ninja_form id=1]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
