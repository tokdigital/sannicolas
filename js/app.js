AOS.init();

$(document).foundation();

  $(".show-canvas").click(function(){
    $(".off-canvas-menu").animate({"left":"0"},300);
    $(".canvas-container").css("overflow","visible");
    $('body').toggleClass('overflow');
    $(".header").css("overflow","visible");

  });
  $(".close-button , .off-canvas-menu li a").click(function(){
    $(".off-canvas-menu").animate({"left":"100%"},300);
    $(".canvas-container").css("overflow","hidden");
    $('body').toggleClass('overflow');
    $(".header").css("overflow","hidden");
  });

$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

var winScrollTop=0;

$.fn.is_on_screen = function(){    
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    //viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    //bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};
function parallax() { 
  var scrolled = $(window).scrollTop();
  $('.parallax-section').each(function(){ 
  
     if ($(this).is_on_screen()) {  
          var firstTop = $(this).offset().top; 
          var $span = $(this).find(".moving-block");
          var $small = $(this).find(".moving-block2");
          var moveTop = -(firstTop-winScrollTop)*0.05 //speed;
          var moveTopSlow = -(firstTop-winScrollTop)*0.05 //speed;
          var moveDown = -(firstTop-winScrollTop)*0.05 //speed;
          $span.css("transform","translateY("+-moveTop+"px)");
          $small.css("transform","translateY("+-moveDown+"px)");
     }
     
  });
  }

  $(window).scroll(function(e){
  winScrollTop = $(this).scrollTop();
  parallax();
});


 

$('#red-com.selector .selector-controllers li').each(function(){
  $(this).click(function(){
    var x = $(this).data('value');
    $('#red-com .selector-controllers li').removeClass('active');
    $(this).addClass('active');
    $('#red-com .selector-properties .single-property').removeClass('active');
    $('#red-com .selector-properties .single-property[data-property="'+x+'"]').addClass('active');
  })
})
$('#red-props.selector .selector-controllers li').each(function(){
  $(this).click(function(){
    var x = $(this).data('value');
    $('#red-props .selector-controllers li').removeClass('active');
    $(this).addClass('active');
    $('#red-props .selector-properties .single-property').removeClass('active');
    $('#red-props .selector-properties .single-property[data-property="'+x+'"]').addClass('active');
  })
})

var lastScrollTop = 0;
window.addEventListener("scroll", function(){  
   var st = window.pageYOffset || document.documentElement.scrollTop;  
   if (st > lastScrollTop){
       // document.getElementById("scroll-menu").style.top = "-100%";
       if (document.getElementById('scroll-menu').classList.contains('transparent')) {
        document.getElementById("scroll-menu").style.background = "rgba(215, 13, 13, 0.43)";
      }
   } else {
      // document.getElementById("scroll-menu").style.top = "0";
      if (document.getElementById('scroll-menu').classList.contains('transparent')) {
        document.getElementById("scroll-menu").style.background = "rgba(215, 13, 13, 0.43)";
      }
   }
   lastScrollTop = st;
}, false); 


$('.open-universal').on('click', function(){
  $('.universal-form-pop-up').addClass('open');
  $('body').addClass('hidden');
});

$('.universal-form-pop-up .close-form-pop').on('click', function(){
  $('.universal-form-pop-up').removeClass('open');
  $('body').removeClass('hidden');
})





// filTERez
var valueKategoria;
// var selectKategoria = document.getElementById('kategoria');
var priceMonth = document.getElementById('price-slider-month');
var priceMeter = document.getElementById('price-slider-meter');

var selectKategoria = document.getElementsByName('kategoria');
if (selectKategoria.length) {
var rad = document.filter.kategoria;
var prev = null;
for (var i = 0; i < rad.length; i++) {
    rad[i].addEventListener('change', function() {
        (prev) ? console.log(prev.value): null;
        if (this !== prev) {
            prev = this;
        }
        valueKategoria = this.value;
        if (valueKategoria == 'me-qera') {
          priceMonth.classList.remove('hide');
          priceMeter.classList.add('hide');
        } else {
          priceMeter.classList.remove('hide');
          priceMonth.classList.add('hide');
        }
    });
}
}

$('.radio').on('click', function(){
  var radioNum = $(this).attr('data-rad');
  $('[name="kategoria"][value="'+radioNum+'"]').click();

  $('.radio').removeClass('active');

  $(this).addClass('active');
})


var lastScrollTop = 0;
$(window).scroll(function(event){
   var st = $(this).scrollTop();
   if (st > lastScrollTop){
       $('.header').addClass('active');
   } else if (st == 0) {
      $('.header').removeClass('active');
   }
   
   lastScrollTop = st;
});


var video = document.getElementById('video');

$('#learn-more').on('click', function() {
  $('#video-overlay').addClass('open');
  video.play();
});

$('#play-button').on('click', function() {
  $('#video-overlay').addClass('open');
  video.play();
});

$('#video-overlay .close-button').on('click', function() {
  $('#video-overlay').removeClass('open');
  video.stop();
});


$('#overlay-input').on('click', function() {
  $('#mobile-overlay').toggleClass('active');
});


const textSwiper = new Swiper('#text-slider', {
  loop: true,
  effect: 'fade',
  fadeEffect: { crossFade: true },
});

const imageSwiper = new Swiper('#image-slider', {
  // Optional parameters
  loop: true,
  effect: 'fade',
  fadeEffect: { crossFade: true },
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

});

textSwiper.controller.control = imageSwiper;
imageSwiper.controller.control = textSwiper;




setTimeout(function(){
  hidePreloader();
}, 1000);

function hidePreloader() {
  $('.preloader').addClass('hide-preloader');
}


setTimeout(function() {
  removePreloader();
}, 2000);
function removePreloader() {
  $('.preloader').hide();
}