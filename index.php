<?php get_header(); ?>

<section class="home-slider">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="home-slider-wrapper">
					<div class="single-slide"><img src="<?php bloginfo('template_url') ?>/img/slider-1.png" alt=""></div>
					<div class="single-slide"><img src="<?php bloginfo('template_url') ?>/img/slider-1.png" alt=""></div>
					<div class="single-slide"><img src="<?php bloginfo('template_url') ?>/img/slider-1.png" alt=""></div>
					<div class="single-slide"><img src="<?php bloginfo('template_url') ?>/img/slider-1.png" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="introduction">
	<div class="decoration pos-3">
		<img src="<?php bloginfo('template_url') ?>/img/decoration.svg" alt="">
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-7">
				<h2 class="section-title">Introduction</h2>
				<p class="section-subtitle">Signalball will jump you to the next level</p>
				<ul class="list">
					<li>Wide range offer of In-Play and Pre-Match odds</li>
					<li>Numerous main and side markets</li>
					<li>35 different sports, 150,000 Pre-Match, 30,000 In-Play</li>
					<li>Lowest latency in the market</li>
					<li>One data only will provide all the statistics, livescore, odds, market settlement and fixtures</li>
					<li>Very intuitive and efficient backend</li>
					<li>More than 100 different bookmakers  coverage</li>
					<li>Languages 45</li>
				</ul>
			</div>
			<div class="cell medium-5">
				<div class="cubes-image">
					<img src="<?php bloginfo('template_url') ?>/img/cubes.png" alt="">
				</div>
				<a href="#" class="button">View More</a>
			</div>
		</div>
	</div>
</section>

<section class="colored how-it-works">
	<div class="grid-container">
		<div class="grid-x align-center text-center">
			<div class="cell medium-12">
				<h2 class="section-title ">How it works</h2>
				<p class="section-subtitle ">SignalBall, solid cross interactivity</p>
			</div>
			<div class="cell medium-9">
				<img src="<?php bloginfo('template_url') ?>/img/how-it-works.jpg" alt="">
				<a href="#" class="button">View More</a>
			</div>
		</div>
	</div>
</section>

<section class="our-strengths">
	<div class="decoration pos-1">
		<img src="<?php bloginfo('template_url') ?>/img/decoration.svg" alt="">
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="strengths">
					<div class="grid-x">
						<div class="cell medium-4 text-center">
							<div class="ball first">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/api.svg" alt=""></div>
								<div class="content"><p>Easy and fast<br>integration via<br>SignalBall API</p></div>
							</div>
							<div class="ball second">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/ball-icon-2.svg" alt=""></div>
								<div class="content"><p>Boost your selling will<br>increase your ROI</p></div>
							</div>
						</div>
						<div class="cell medium-4 text-center">
							<div class="ball third">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/ball-icon-3.svg" alt=""></div>
								<div class="content"><p>Easy and smart<br>match booking<br>system</p></div>
							</div>
						</div>
						<div class="cell medium-4 text-center">
							<div class="ball fourth">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/ball-icon-4.svg" alt=""></div>
								<div class="content"><p>Intelligent Data<br>Analysis</p></div>
							</div>
							<div class="ball fifth">
								<div class="icon"><img src="<?php bloginfo('template_url') ?>/img/ball-icon-5.svg" alt=""></div>
								<div class="content"><p>Pay as you Book</p></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell medium-6 text-right">
				<h2 class="section-title">Our strengths</h2>
				<p class="section-subtitle">SignalBall, constantly on the top of the signal</p>
				<a href="#" class="button">View More</a>
			</div>
		</div>
	</div>
</section>

<section class="colored head-to-head">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-12 text-center">
				<h2 class="section-title">Head to Head</h2>
				<p class="section-subtitle">Lorem Ipsum Dolor Sit Amet</p>
				<div class="section-decoration"><img src="<?php bloginfo('template_url') ?>/img/section-decoration.svg" alt=""></div>
			</div>
			<div class="cell medium-9">
				<div class="head-to-head-slider">
					<div class="single-slide">
						<div class="grid-x">
							<div class="cell medium-4">
								<img class="player1" src="<?php bloginfo('template_url') ?>/img/player1.png" alt="">
							</div>
							<div class="cell medium-4">
								<div class="middle-text">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et  dolore magna aliqua. 
								</div>
							</div>
							<div class="cell medium-4">
								<img class="player2" src="<?php bloginfo('template_url') ?>/img/player2.png" alt="">
							</div>
							<div class="cell medium-12">
								<div class="names">
									<div class="grid-x">
										<div class="cell medium-5 small-5">
											<div class="team-name home">Barcelona</div>
										</div>
										<div class="cell medium-2">
											<div class="vs-text">
												vs
											</div>
										</div>
										<div class="cell medium-5 small-5">
											<div class="team-name away">Real Madrid</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="single-slide">
						<div class="grid-x">
							<div class="cell medium-4">
								<img class="player1" src="<?php bloginfo('template_url') ?>/img/player1.png" alt="">
							</div>
							<div class="cell medium-4">
								<div class="middle-text">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et  dolore magna aliqua. 
								</div>
							</div>
							<div class="cell medium-4">
								<img class="player2" src="<?php bloginfo('template_url') ?>/img/player2.png" alt="">
							</div>
							<div class="cell medium-12">
								<div class="names">
									<div class="grid-x">
										<div class="cell medium-5 small-5">
											<div class="team-name home">Barcelona</div>
										</div>
										<div class="cell medium-2">
											<div class="vs-text">
												vs
											</div>
										</div>
										<div class="cell medium-5 small-5">
											<div class="team-name away">Real Madrid</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</section>

<section class="sports-curiosity">
	<div class="decoration pos-2">
		<img src="<?php bloginfo('template_url') ?>/img/decoration.svg" alt="">
	</div>
	<div class="decoration pos-4">
		<img src="<?php bloginfo('template_url') ?>/img/decoration.svg" alt="">
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-4">
				<h2 class="section-title text-left">Sports Curiosity</h2>
				<ul class="slider-controls">
					<li>NFL <span></span></li>
					<li>NBA <span></span></li>
					<li>RACING <span></span></li>
					<li>ODDS <span></span></li>
					<li>MLB <span></span></li>
					<li>MMA <span></span></li>
					<li>SOCCER <span></span></li>
				</ul>
			</div>
			<div class="cell medium-8">
				<div class="sports-slider">
					<div class="single-slide">
						<div class="single-post-holder">
							<img src="<?php bloginfo('template_url') ?>/img/match.jpg" alt="">
							<div class="post-content">
								<div class="grid-x">
									<div class="cell medium-9">
										<div class="title">6 surprise players who might jump up the NFL draft board</div>
									</div>
									<div class="cell medium-3">
										<a href="#" class="button">View More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="technology colored">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<h2 class="section-title">Technology</h2>
				<p class="section-subtitle">SignalBall, technologies owner</p>
				<ul class="list tick2">
					<li>State of the art, proprietary platform</li>
					<li>Fast and seamless integration (plug & play)</li>
					<li>Multiple integration options</li>
					<li>Jam packed API</li>
					<li>Fully customizable</li>
					<li>Support all languages</li>
					<li>Includes back office & reporting</li>
				</ul>
			</div>
			<div class="cell medium-6 text-center">
				<img src="<?php bloginfo('template_url') ?>/img/tech.jpg" alt="">
				<a href="#" class="button">View More</a>
			</div>
		</div>
	</div>
</section>

<section class="responsive">
	<div class="decoration small pos-2">
		<img src="<?php bloginfo('template_url') ?>/img/decoration.svg" alt="">
	</div>
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-6">
				<img class="medias" src="<?php bloginfo('template_url') ?>/img/responsive.png" alt="">
				<div class="border-box"></div>
				<div class="arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-right-orange.svg" alt=""></div>
			</div>
		</div>
	</div>
</section>

<section class="quote">
	<img src="<?php bloginfo('template_url') ?>/img/quote.jpg" alt="">
	<a href="#" class="button blue">Get a Quote</a>
</section>

<?php get_footer(); ?>

