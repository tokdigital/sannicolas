<?php /* Template Name: Contact */ ?>

<?php get_header(); ?>

<section class="section-contact">
    <div class="container-wrapper"> 
        <div class="grid-container">
            <div class="contact-inner" data-aos="fade-up" data-aos-duration="2500">
                <div class="grid-x grid-padding-x">

                    <div class="cell medium-6" data-aos="fade-up" data-aos-duration="2500">
                        <article>
                            <h1>Get in Touch</h1>
                            <p>Fill up the form and our Team will<br> get back to you within 24 hours.</p>
                            <p><i class="fas fa-phone"></i> <a href="tel:+355672077368">067 20 77 368</a></p>
                            <p><i class="fas fa-envelope"></i> <a href="mailto:info@sannicolas.al">info@sannicolas.al</a></p>
                            <p><i class="fas fa-map-pin"></i> Palase, Dhermi</p>
                            <div class="socials">
                                <a href="https://www.facebook.com/San-Nicolas-Resort-Residences-106423758390736" target="_blank" class="fab fa-facebook-square"></a>
                                <a href="https://www.linkedin.com/company/san-nicolas-resort-residences/" target="_blank" class="fab fa-linkedin"></a>
                                <a href="https://www.instagram.com/sannicolas.al/" target="_blank" class="fab fa-instagram"></a>
                            </div>
                        </article>
                    </div>
                    <div class="cell medium-6" data-aos="fade-up" data-aos-duration="2500" data-aos-delay="700">
                        <div class="form-wrapper">
                            <?php echo do_shortcode('[ninja_form id=1]'); ?>
                        </div>
                    </div>
                    
                </div>
            </div>      
        </div>
    </div>
</section>

<?php get_footer(); ?>
